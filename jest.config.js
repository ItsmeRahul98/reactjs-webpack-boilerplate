module.exports = {
  transform: { '^.+\\.[t|j]sx?$': 'babel-jest' },
  testEnvironment: 'jsdom',
  modulePathIgnorePatterns: [
    '<rootDir>/build/',
    '<rootDir>/public/',
    '<rootDir>/dist/',
    '<rootDir>/coverage/',
    '<rootDir>/node_modules/',
    '<rootDir>/config/',
    '<rootDir>/jest.config.js',
    '<rootDir>/postcss.config.js',
    '<rootDir>/.eslintrc.js',
    '<rootDir>/__mock__/',
    '<rootDir>/src/index.js',
    '<rootDir>/generators',
    '<rootDir>/.storybook/',
    '<rootDir>/src/sampleStories/',
    '(.stories)\\.(ts|jsx|js)$'
  ],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|less|scss)$': '<rootDir>/__mocks__/styleMock.js'
  },
  transformIgnorePatterns: [
    '<rootDir>/build/',
    '<rootDir>/public/',
    '<rootDir>/dist/',
    '<rootDir>/coverage/',
    '<rootDir>/config/',
    '<rootDir>/jest.config.js',
    '<rootDir>/node_modules/',
    '<rootDir>/postcss.config.js',
    '<rootDir>/.eslintrc.js',
    '<rootDir>/__mock__/',
    '<rootDir>/src/index.js',
    '<rootDir>/generators',
    '<rootDir>/.storybook/',
    '<rootDir>/src/sampleStories/',
    '(.stories)\\.(ts|jsx|js)$'
  ],
  coveragePathIgnorePatterns: ['/node_modules/', '(.stories)\\.(ts|jsx|js)$'],
  testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/build/'],
  testResultsProcessor: 'jest-sonar-reporter',
  watchPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/build/'],
  timers: 'fake',
  collectCoverage: true,
  collectCoverageFrom: ['**/*.{js,jsx,ts,tsx}'],
  coverageDirectory: 'coverage',
  coverageReporters: [
    'lcov',
    'json',
    'json-summary',
    'clover',
    'text',
    'text-summary'
  ],
  coverageThreshold: {
    global: {
      branches: 85,
      functions: 85,
      lines: 85,
      statements: 85
    }
  }
}
